<?php
require_once ('vendor/autoload.php');
 require_once "src/query/Query.php";

use query\Query;
use connection\ConnectionFactory;

$conf = parse_ini_file('src/connection/conf.ini');
ConnectionFactory::makeConnection($conf);

echo "<h1>Tous les articles</h1>";
$res = \model\Article::all();

foreach ($res as $v){
    echo '<p>'.$v->nom.'</p>';
}


echo "<h1>Find avec id 66</h1>";

$req = \model\Article::find(66);
var_dump($req);

echo "<h1>Find avec sélection des champs</h1>";

$req = \model\Article::find(66,['id','tarif']);
var_dump($req);

echo "<h1>Find avec WHERE</h1>";

$req = \model\Article::find(['tarif','<=',250],['id','tarif']);
var_dump($req);

echo "<h1>find avec pls critères de recherche</h1>";
$req = \model\Article::find([['nom ','like ','%biclou%'],['tarif','<=',220]]);
var_dump($req);

echo "<h1>Premier article à moins de 250 </h1>";
$req = \model\Article::first(['tarif','<=',250]);
var_dump($req);


echo "<h1>Categorie associé à un article</h1>";

$a= \model\Article::first(66);
$categorie = $a->categorie();
var_dump($categorie);

echo "<h1>Articles associés à une categorie</h1>";
$c = \model\Categorie::first(1);
$articles = $c->articles();
var_dump($articles);


